import numpy as np
import matplotlib.pyplot as plt
from scipy import stats

line = '-----'
estaturas=[1.59,1.67,1.55,1.76,1.6,1.95,1.65,1.82,1.99,1.55,1.67,1.72,1.82,1.72,
    1.63,1.75,1.79,1.73,1.63,1.77,1.75,1.63,1.73,1.81,1.6,1.57,1.68,1.76,
    1.58,1.71,1.7,1.79,1.6,1.65,1.68,1.64,1.62,1.7,1.6,1.47,1.5,1.53,1.55,
    1.57,1.6,1.83,1.7,1.57,1.6,1.83,1.8,1.63,1.54,1.87,1.58,1.47,1.54,1.62,
    1.68,1.75,1.83,1.73,1.5,1.66,1.78,1.75,1.78,1.7,1.73,1.7,1.8,1.75]

pesos=[55,67,58,70,85,102,66,82,115,60,60,70,96,71,65,70,78,73,64,74,80,65,
    64,69,58,50,54,77,56,68,75,75,66,70,62,58,65,68.5,56.3,55,56,57,59,
    60,61,74,67,64,65,70,88,56,60,80,57,39.5,52,65,72,66,78,68,
    48,56,74,70,89,69,63,78,79,65]

#Gracias a Numpy puedo hacer operaciones entre Arrays sin el uso de FOR
estaturas=np.array(estaturas)
pesos=np.array(pesos)
imcs=pesos/estaturas**2


"""Graficar las Estaturas Vrs Pesos """
#plt.scatter(estaturas,pesos)

"""Diagrama de caja y bigotes""" 
#plt.boxplot(estaturas)
#plt.boxplot(pesos)
#plt.boxplot(imcs)

#Usar clf() para limpiar el grafico, de lo contrario las graficas se van a superponer
#plt.clf()
#Se puede Guardar Imagen
#plt.savefig("estaturas.png") 

"""Medidades Centrales""" 
mediana = np.median(imcs)
media = np.mean(imcs)
desviacion_estandar = np.std(imcs)

limiteInferior = media - desviacion_estandar
limiteSuperior = media + desviacion_estandar

print('Teniendo en cuenta la Regla Empirica significa que el 68% de los datos se encuentran entre ' + 
      str(limiteInferior) + ' y ' + str(limiteSuperior) + '.')

"""Histogramas de Frecuencias"""
#plt.hist(imcs, bins=7)  
#plt.hist(imcs, bins=150, cumulative=True)


"""Definir los Filtros para poder estimar como esta Segun su IMC"""
imcsEnclenque=(imcs<18.5)
imcsNormi=(imcs>=18.5)&(imcs<=24.9)
imcsSobrepeso=(imcs>24.9)&(imcs<=29.9)
imcsGordo=(imcs>29.9)

"""Clasificar los datos segun el IMC"""
pesosEnclenque = pesos[imcsEnclenque]
estaturasEnclenque = estaturas[imcsEnclenque]

pesosNormi = pesos[imcsNormi]
estaturasNormi = estaturas[imcsNormi]

pesosSobrepeso = pesos[imcsSobrepeso]
estaturasSobrepeso = estaturas[imcsSobrepeso]

pesosGordo = pesos[imcsGordo]
estaturasGordo = estaturas[imcsGordo]

"""Graficar los datos Segun Clasificacion"""
#plt.scatter(estaturasEnclenque, pesosEnclenque, c="green")
#plt.scatter(estaturasNormi, pesosNormi , c="blue")
#plt.scatter(estaturasSobrepeso,pesosSobrepeso , c="yellow",alpha=0.8)
#plt.scatter(estaturasGordo,pesosGordo , c="red")


"""Coeficiente de Correlacion"""
print(line)
print('Coeficiente de Correlacion IMCs - Pesos')
coeficienteCorr_pesos = np.corrcoef(imcs,pesos)
print(coeficienteCorr_pesos)
print(line)
print('Coeficiente de Correlacion IMCs - Estaturas')
coeficienteCorr_estaturas = np.corrcoef(imcs, estaturas )
print(coeficienteCorr_estaturas)


"""Regresion Lineal"""

#Calcula la pendiente y el intercepto segun los datos
pendiente, intercepto, r,p,e=stats.linregress(estaturas,pesos)

def regresionLineal(estatura):
    peso = pendiente*estatura + intercepto
    return peso

#plt.plot(estaturas, regresionLineal(estaturas))


"""Multiples Graficos"""
figura = plt.figure()

#Histogramas de Frecuencias
figHistograma=figura.add_subplot(221, facecolor='y')
figHistograma.set_title('Histograma IMCs')
figHistograma.hist(imcs, bins=7) 

figHistogramaAcum=figura.add_subplot(222, facecolor='y')
figHistogramaAcum.set_title('Histograma Acumulativo IMCs')
figHistogramaAcum.hist(imcs, bins=150, cumulative=True)

graficaDatosClasificados=figura.add_subplot(223)
graficaDatosClasificados.set_title('Datos Clasificados')
graficaDatosClasificados.scatter(estaturasEnclenque, pesosEnclenque, c="green")
graficaDatosClasificados.scatter(estaturasNormi, pesosNormi , c="blue")
graficaDatosClasificados.scatter(estaturasSobrepeso,pesosSobrepeso , c="yellow",alpha=0.8)
graficaDatosClasificados.scatter(estaturasGordo,pesosGordo , c="red")

graficaRegresionLineal = figura.add_subplot(224)
graficaRegresionLineal.set_title('Regresion Lineal')
graficaRegresionLineal.plot(estaturas, regresionLineal(estaturas)) 

#plt.show()
"""Predecir Peso"""

print(line)
#estaturaX = float(input('Ingrese su estatura: '))
estaturaX = 1.87
print('Su peso es: ', regresionLineal(estaturaX))

"""Graficos en 3D"""

#Necesito un tercer Argumento
imcsEnclenque_z = imcs[imcsEnclenque]
imcsNormi_z  = imcs[imcsNormi]
imcsSobrepeso_z  = imcs[imcsSobrepeso]
imcsGordo_z  = imcs[imcsGordo]


figura3D = plt.figure()

graficaDatosClasificados3D = figura3D.add_subplot(1,1,1,  projection='3d')
graficaDatosClasificados3D.scatter(estaturasEnclenque, pesosEnclenque, imcsEnclenque_z)
graficaDatosClasificados3D.scatter(estaturasNormi, pesosNormi, imcsNormi_z)
graficaDatosClasificados3D.scatter(estaturasSobrepeso, pesosSobrepeso, imcsSobrepeso_z)
graficaDatosClasificados3D.scatter(estaturasGordo, estaturasGordo, imcsGordo_z)
